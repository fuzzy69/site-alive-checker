#include <QDesktopWidget>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QShowEvent>
#include <QCloseEvent>
#include <QResizeEvent>
#include <QSettings>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sitesModel = new QStandardItemModel;
    sitesModel->setHorizontalHeaderLabels(QStringList() << "URL" << "Result" << "Code" << "Status");
    ui->sitesTable->setModel(sitesModel);
    applicationDirPath = QApplication::applicationDirPath();
    currentDir = applicationDirPath;
    settingsFilePath = QDir(applicationDirPath).absoluteFilePath("data/settings.ini");
    readSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_quitAction_triggered()
{
    qApp->quit();
}

void MainWindow::on_importUrlsAction_triggered()
{
    QFileDialog::getOpenFileName(this, "Import URLs", currentDir, "Text files (*.txt);; All files (*.*)");
}

void MainWindow::on_exportResultsAction_triggered()
{
    QFileDialog::getSaveFileName(this, "Export results", currentDir, "CSV files (*.csv);; JSON files (*.json)");
}

void MainWindow::showEvent(QShowEvent *event)
{
    resizeTableColumns();
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    resizeTableColumns();
    event->accept();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}

void MainWindow::resizeTableColumns()
{
    ui->sitesTable->setColumnWidth(0, static_cast<int>(ui->sitesTable->frameGeometry().width() * 0.6));
}

void MainWindow::readSettings()
{
    QSettings settings(settingsFilePath, QSettings::IniFormat);
    restoreGeometry(settings.value("MainWindowGeometry").toByteArray());
    restoreState(settings.value("MainWindowState").toByteArray());
}

void MainWindow::writeSettings()
{
    QSettings settings(settingsFilePath, QSettings::IniFormat);
    settings.setValue("MainWindowGeometry", saveGeometry());
    settings.setValue("MainWindowState", saveState());
}

void MainWindow::centerWindow()
{
    QRect fg = this->frameGeometry();
    QDesktopWidget dw;
    QPoint c = dw.availableGeometry().center();
    fg.moveCenter(c);
    this->move(fg.topLeft());
}

void MainWindow::on_centerWindowAction_triggered()
{
    centerWindow();
}
