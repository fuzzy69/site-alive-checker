#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QStandardItemModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void resizeTableColumns();
    void readSettings();
    void writeSettings();
    void centerWindow();

private slots:
    void on_quitAction_triggered();
    void on_importUrlsAction_triggered();
    void on_exportResultsAction_triggered();
    void on_centerWindowAction_triggered();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *sitesModel;
    QString currentDir;
    QString applicationDirPath;
    QString settingsFilePath;

protected:
    void showEvent(QShowEvent *event);
    void closeEvent(QCloseEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif // MAINWINDOW_H
